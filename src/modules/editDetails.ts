
let eventOnEdit = (addListener: boolean)=>{
    addListener ? $('.edit-details').on("click", alterDetails) : $('.edit-details').off()
};

let alterDetails = (e: any)=>{
    let $item = $(e.target).parents('tr');
    $item.find(".update, .edit").toggle()
    $item.find("td input").prop("disabled", (index, bool)=>(!bool));
    $item.find("td input").each((index, item: any)=>(item.setAttribute("value", item.value))
}

export {alterDetails, eventOnEdit}