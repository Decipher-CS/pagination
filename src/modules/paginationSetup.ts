import {data as userData} from "../user_data_obj.js"
import { alterDetails, eventOnEdit } from "./editDetails.js";

const listElementTemplate:any = $('.template-list-item').clone()[0]

let clearItemList = ()=>{$('tbody').empty()};

let appendItemsToList = (pageNumber: number)=>{
    clearItemList()
    let [startFrom, endAt] = [(pageNumber*10)- 10, pageNumber* 10]
    for (let item = startFrom; item < endAt; item++){
        $('tbody').append((addItemDetails(userData[item])))
    }
    return 1
};


let addItemDetails = ({id, name, email, title})=>{
    let node = listElementTemplate.content.cloneNode(true).firstElementChild
    $(node).find('td.id div').text(id)
    $(node).find('td.id div').attr('value', id)
    $(node).find('td.name input').attr('value', name)
    $(node).find('td.email input').attr('value', email)
    $(node).find('td.title input').attr('value', title)
    return node
};

let pageChangeBtnClicked = (e: any)=>{
    let currPage =  $('#currentPage')
    let currPageNumber = <number>  Number(currPage.attr("value"))
    // $('.edit-details').off()
    
    if ( (e.target.classList.contains("previous")) || (e.target.getAttribute("alt") === "Previous") ){
        if (currPageNumber != 1){
            currPage.attr("value", currPageNumber-1)
            appendItemsToList(currPageNumber-1)
        }
    }
    if ( (e.target.classList.contains("next")) || (e.target.getAttribute("alt") === "Next") ){
        if (currPageNumber != 3){
            currPage.attr("value", currPageNumber+1)
            appendItemsToList(currPageNumber+1)
        }
    };
    eventOnEdit(false);
    eventOnEdit(true);
};

export {appendItemsToList, pageChangeBtnClicked}