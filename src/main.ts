import {data as userData} from "./user_data_obj.js"
import "./modules/jquery.js"
import {appendItemsToList ,pageChangeBtnClicked} from "./modules/paginationSetup.js"
import {alterDetails, eventOnEdit} from "./modules/editDetails.js"
import { listenForSort, sort } from "./modules/detailSorting.js"


$(()=>{
    appendItemsToList(1);
    $('button.previous, button.next').on("click", pageChangeBtnClicked);
    eventOnEdit(true)
    listenForSort()
)