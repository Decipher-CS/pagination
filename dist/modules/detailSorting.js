let listenForSort = () => {
    $('.sort').on("click", sort);
};
let sort = (e) => {
    let target = e.target;
    let sortCategory = $(target).parents("th")[0].classList[0];
    let sortOrderBtn = $(target).parents("button.sort");
    if (target.classList.contains("ascending")) {
        sortInAscendingOrder(sortCategory);
        sortOrderBtn.addClass("ascending").removeClass("descending");
        return;
    }
    else if (target.classList.contains("descending")) {
        sortInDescendingOrder(sortCategory);
        sortOrderBtn.addClass("descending").removeClass("ascending");
    }
};
let sortInAscendingOrder = (field) => {
    let isAscending = true; //Don't judge, I am being explicit here.
    sorter(field, isAscending);
};
let sortInDescendingOrder = (field) => {
    let isAscending = false; //Don't judge, I am being explicit here.
    sorter(field, isAscending);
};
let getValueAtIndex = (index, field) => {
    return $(`tbody td.${field} *`).get(index).getAttribute("value");
};
let swapItems = (swapThis, withThis) => {
    let original = $($('tbody tr')[swapThis]).replaceWith($('tbody tr')[withThis]);
    $('tbody tr')[withThis - 1].after(original[0]);
};
// let getBigger = (x: number | string, y: number | string) => {x > y}
let getBigger = (x, y) => {
    x = isNaN(x) ? x.toUpperCase() : Number(x);
    y = isNaN(y) ? y.toUpperCase() : Number(y);
    return (x > y);
};
let sorter = (field, isAscending) => {
    for (let outerIndex = 0; outerIndex < 10; outerIndex++) {
        for (let innerIndex = 0; innerIndex < 9; innerIndex++) {
            let value1 = getValueAtIndex(innerIndex, field);
            let value2 = getValueAtIndex(innerIndex + 1, field);
            if (getBigger(value1, value2) === isAscending) {
                swapItems(innerIndex, innerIndex + 1);
            }
        }
    }
};
export { sort, listenForSort };
