_**Note** : I'll keep it short_

### Advent of JavaScript : Challenge #24

#### Why did make it?
- I wanted to get more familiar with TS and JQuery. Implementing the following helped me get there.
    - Sorting
    - State management for different pages
    - Dynamic editing/ data amending

#### New technology Learned
- JQuery
- TypeScript

#### What I used
- HTML/CSS
- TypeScript
- JQuery

#### Continued Development
- Fundamental understanding of TS
- JQ is versatile. Need to lean what more it is capable of.
- Configuring TS and JQ for deployment. 
  - Bundle files into smaller packages.
  - Compress files.
  - etc.

#### Live Website
- [Pagination URL](https://animated-elf-4e758d.netlify.app/)
- [GitLab Repo](https://gitlab.com/webdev-challenges/pagination)